package com.company;

import kolektiv.Zaposlen;
import kolektiv.rad_sa_Datotekama;
public class Main {

    public static void main(String[] args) {
        Zaposlen[] kolektiv = {
                new Zaposlen("Marko", "Djurdjevic", 1995, "Miloja Milankovica123", "Menadzment", 50000),
                new Zaposlen("Aca", "Djurdjevic", 1995, "Miloja Milankovica123", "Menadzment", 500400),
                new Zaposlen("Rest", "Djurdjevic", 1995, "Miloja Milankovica123", "Menadzment", 52000)
        };

        for (Zaposlen zaposlen: kolektiv) {
            System.out.println(zaposlen);
        }

        Zaposlen.pronadjiZaposlenog(kolektiv);

        System.out.println("\nSortirani zaposleni");
        for (Zaposlen zaposlen: Zaposlen.sortirajZaposlene(kolektiv)) {
            System.out.println(zaposlen);
        }
        System.out.println("\nUpisani zaposleni");
        for(Zaposlen z:kolektiv){
            z.upisiUDatoteku("zaposleni.txt");
        }
        System.out.println("\nIspisani zaposleni");

        kolektiv[0].ispisSadrzaja("zaposleni.txt");
    }
}
