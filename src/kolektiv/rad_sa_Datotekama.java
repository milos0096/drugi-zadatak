package kolektiv;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;

public interface rad_sa_Datotekama {
    public void upisiUDatoteku(String imeDat);
    public  void ispisSadrzaja(String imeDat);
}
