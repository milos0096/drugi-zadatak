package kolektiv;

import java.io.*;
import java.util.ArrayList;

public class Zaposlen extends Osoba implements rad_sa_Datotekama{
    private int id;
    private String odeljenje;
    private int plata;
    private static int pocetniId = 0;

    public Zaposlen(String ime, String prezime, int godiste, String adresa, String odeljenje, int plata) {
        super(ime, prezime, godiste, adresa);

        this.odeljenje = odeljenje;
        this.plata = plata;
        this.id =++pocetniId;
    }

    public int vratiPlatu() {
        return plata;
    }
    public String vratiIme() {return ime;}

    public static Zaposlen[] sortirajZaposlene(Zaposlen[] nizZap){

        for (int i=0;i<nizZap.length-1;i++) {
            for(int j=i+1;j<nizZap.length;j++){
                if(nizZap[i].vratiIme().compareTo(nizZap[j].vratiIme()) > 0){
                    Zaposlen tmp = nizZap[i];
                    nizZap[i] = nizZap[j];
                    nizZap[j] = tmp;
                }
            }
        }

        return nizZap;
    }

    public static void pronadjiZaposlenog(Zaposlen[] nizZap) {
        Zaposlen maxP=nizZap[0];
        for (int i=1; i<nizZap.length;i++) {
            if(maxP.vratiPlatu() < nizZap[i].vratiPlatu()){
                maxP = nizZap[i];
            }
        }
        System.out.println("Najvecu platu ima" + maxP);
    }

    @Override
    public String toString() {
        return super.toString() + "Zaposlen{" +
                "id=" + this.id +
                ", odeljenje='" + this.odeljenje + '\'' +
                ", plata=RSD" + this.plata +
                "} " ;
    }


    @Override
    public void upisiUDatoteku(String imeDat) {
        File f = new File(imeDat);
        if(!f.exists()) {
            try{
                f.createNewFile();
            }catch(IOException e){
                e.printStackTrace();
            }
        }

        try {
            FileWriter fw = new FileWriter(imeDat,true);
            PrintWriter pw = new PrintWriter(fw);
            pw.print(this.toString() + System.getProperty("line.separator"));
            fw.close();
            pw.close();
            System.out.println("Uspesno snimljen zaposlen:" + this.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ispisSadrzaja(String imeDat) {
        try {
            FileInputStream fis = new FileInputStream(imeDat);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            ArrayList<String> sadrzaj = new ArrayList<>();
            String linija = br.readLine();
            while(linija!=null) {
                sadrzaj.add(linija);
                linija=br.readLine();
            }
            for (String red:sadrzaj){
                System.out.println(red);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
