package kolektiv;

public abstract class Osoba {
    protected String ime;
    protected String prezime;
    protected int godiste;
    protected String adresa;


    public Osoba(String ime, String prezime, int godiste, String adresa) {
        this.ime = ime;
        this.prezime = prezime;
        this.godiste = godiste;
        this.adresa = adresa;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "ime='" + this.ime + '\'' +
                ", prezime='" + this.prezime + '\'' +
                ", godiste=" + this.godiste +
                ", adresa='" + this.adresa + '\'' +
                '}';
    }


}
